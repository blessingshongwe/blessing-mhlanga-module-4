import 'package:flutter/material.dart';

class MyThemes {
  static final lightTheme = ThemeData(

    // AppBar Theme
    appBarTheme: AppBarTheme(
      color: Colors.transparent,
      elevation: 0,
      iconTheme: const IconThemeData(color: Colors.pink),
      toolbarTextStyle: const TextTheme(
        headline6: TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ).bodyText2, titleTextStyle: const TextTheme(
        headline6: TextStyle(
          color: Colors.blueGrey,
          fontSize: 20,
          fontWeight: FontWeight.bold,
          fontFamily: 'Roboto',
        ),
      ).headline6,
    ),

    //Chip Theme
    chipTheme: const ChipThemeData(
      labelStyle: TextStyle(
        color: Colors.white,
      ),
    ),

    primaryColor: Colors.purple,
    brightness: Brightness.light,
    backgroundColor: Colors.white,
    iconTheme: const IconThemeData(
      color: Colors.pink,
      size: 30,
    ),

    // Button Theme
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.teal[400]),
      ),
    ),

    textTheme: const TextTheme(
      headline6: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        fontFamily: 'Roboto',
      ),
    ),

  //  Card Theme
    cardTheme: CardTheme(
        elevation: 10,
        color: Colors.white70,
        shadowColor: Colors.deepPurple[900],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
      ),
    );

static final darkTheme = ThemeData(
  appBarTheme: AppBarTheme(
    backgroundColor: Colors.transparent,
    elevation: 0,
    iconTheme: const IconThemeData(color: Colors.pink),
    toolbarTextStyle: const TextTheme(
      headline6: TextStyle(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
    ).bodyText2, titleTextStyle: const TextTheme(
    headline6: TextStyle(
      color: Colors.white,
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
    ).headline6,
  ),
  brightness: Brightness.dark,
  backgroundColor: Colors.black,
  iconTheme: const IconThemeData(
    color: Colors.purple,
  ),
  textTheme: const TextTheme(
    headline6: TextStyle(
      fontSize: 20,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
  ),

);

}

