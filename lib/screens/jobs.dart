import 'package:assignment/screens/profile-page.dart';
import 'package:flutter/material.dart';

import 'dashboard.dart';
import 'job_details.dart';
import 'login.dart';

class Job {
  final String company;
  final String title;
  final String avatar;
  final String description;
  final String salary;
  final String location;

  const Job({
    required this.company,
    required this.title,
    required this.avatar,
    required this.description,
    required this.salary,
    required this.location,
  });
}

class JobsPage extends StatelessWidget {

  final List<Job> job = [
    const Job(
      company: 'Google',
      title: 'Software Engineer',
      avatar: 'assets/companies/google.png',
      description: "The software engineer job description starts with background requirements. Someone looking to get into this role will need a bachelor’s degree in software, math, or science. Applicants would also be expected to have broad",
      salary: '\$100k - \$200k',
      location: 'New York, NY',
    ),
    const Job(
      company: 'Netflix',
      title: 'Software Engineer',
      avatar: 'assets/companies/001-netflix.png',
      description: '10 years experience in software engineering',
      salary: '\$100k - \$200k',
      location: 'Remote',
    ),
    const Job(
      company: 'Meta',
      title: 'Data Scientist',
      avatar: 'assets/companies/003-meta.png',
      description: '10 years experience in software engineering',
      salary: '\$100k - \$200k',
      location: 'San-francisco, Hybrid',
    ),
    const Job(
      company: 'Huawei',
      title: 'Android App developer',
      avatar: 'assets/companies/005-huawei.png',
      description: '10 years experience in software engineering',
      salary: '\$100k - \$200k',
      location: 'Cape Town, SA',
    ),
  ];

  JobsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Jobs', style: Theme.of(context).textTheme.headline6,),
        actions: <Widget>[
          IconButton(onPressed: (){}, icon: const Icon(Icons.search)),
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
          }, icon: const Icon(Icons.dashboard_rounded)),
          IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Profile()),
                );
              },
              icon: const Icon(Icons.face)
          ),
          IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Login()),
                );
              },
              icon: const Icon(Icons.verified_user_outlined)
          ),
        ],
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: job.length,
              itemBuilder: (context, index) => Card(
                child: Column(
                  children: [
                    ListTile(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => JobDetailsPage(job: job[index])),
                        );
                      },
                      leading: CircleAvatar(
                        radius: 28,
                        backgroundImage: AssetImage(job[index].avatar),
                        backgroundColor: Colors.transparent,
                      ),
                      title: Text('${job[index].company} ${job[index].title}'),
                      subtitle: Text(
                          'Salary : ${job[index].salary} \n${job[index].location}'
                      ),
                      trailing: const Icon(
                        Icons.keyboard_arrow_right,
                        size: 50,
                      ),
                      contentPadding: const EdgeInsets.all(10.0),
                      isThreeLine: true,
                    ),
                  ],
                ),
              )
            ),
          )
        ]
      ),
    );
  }
}

