import 'package:flutter/material.dart';
import 'jobs.dart';

class JobDetailsPage extends StatefulWidget {
  final Job job;
  const JobDetailsPage({Key? key, required this.job}) : super(key: key);




  @override
  State<JobDetailsPage> createState() => _JobDetailsPageState();
}

class _JobDetailsPageState extends State<JobDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Job detail: ''${widget.job.company}'),
          actions: <Widget>[
            Ink(
              decoration: ShapeDecoration(
                color: Colors.lightBlue[200],
                shape: const CircleBorder(),
              ),
              child: IconButton(
                icon: const Icon(Icons.bookmark),
                onPressed: (){},
              ),
            ),
          ],
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
                children: <Widget>[
                  Card(
                    elevation: 10,
                    shadowColor: Colors.deepPurple,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: [
                            const SizedBox(height: 30),
                            CircleAvatar(
                              radius: 30,
                              backgroundImage: AssetImage(widget.job.avatar),
                              backgroundColor: Colors.transparent,
                            ),
                            Text(
                              widget.job.company,
                              style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                            const SizedBox(height: 10),
                            Text(
                              widget.job.title,
                              style: const TextStyle(
                                  fontSize: 24
                              ),
                            ),
                            Text(
                                widget.job.location,
                                style: const TextStyle(
                                    fontSize: 12
                                )
                            ),
                            const SizedBox(height: 10),
                            Text(widget.job.salary , style: const TextStyle(fontSize: 15)),
                            const SizedBox(height: 20),

                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: const [
                      Text(
                        'Description',
                        style: TextStyle(
                          fontSize: 18,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.9,
                          child: Text(widget.job.description,)
                      ),
                    ),
                  ),
                  FractionallySizedBox(
                    widthFactor: 0.8,
                    child: ElevatedButton(
                      onPressed: (){},
                      child: const Text('Apply Now'),
                    ),
                  ),
                ]
            ),
          ),
        )
    );
  }
}
