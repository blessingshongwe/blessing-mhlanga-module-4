import 'package:flutter/material.dart';

import 'jobs.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        actions: <Widget>[
          IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => JobsPage()),
                );
              },
              icon: const Icon(Icons.face)
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset(
                'assets/woman.png',
                fit: BoxFit.cover,
                height: 150,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              child: const Text(
                  'Pepper Potts',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 40,
                ),
              ),
            ),
            const SizedBox(height: 10,),
            const Text('Skills', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const <Widget>[
                Chip(label: Text('HTML5'), backgroundColor: Colors.blue),
                Chip(label: Text('CSS'), backgroundColor: Colors.orange),
                Chip(label: Text('JavaScript'), backgroundColor: Colors.purple),
                Chip(label: Text('Flutter'), backgroundColor: Colors.green),
                Icon(Icons.add),
              ],
            ),
            const SizedBox(height: 10,),
            const Text('Experience', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const ListTile(
              leading: Icon(Icons.work, color: Colors.orange),
              title: Text('Google'),
              subtitle: Text('Senior Software Engineer'),
              trailing: Text('2018 - 2020'),
            ),
            const SizedBox(height: 10,),
            const ListTile(
              leading: Icon(Icons.work, color: Colors.orange),
              title: Text('Self-Employed'),
              subtitle: Text('Free-lance Software Engineer'),
              trailing: Text('2020 - Present'),
            ),
            const SizedBox(height: 10,),
            const Text('Education', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const ListTile(
              leading: Icon(Icons.school, color: Colors.orange),
              title: Text('University of Washington'),
              subtitle: Text('Bachelor of Science'),
              trailing: Text('2019'),
            ),
            const SizedBox(height: 10,),
            const ListTile(
              leading: Icon(Icons.school, color: Colors.orange),
              title: Text('University of Washington'),
              subtitle: Text('Bachelor of Science'),
              trailing: Text('2019'),
            ),
            const SizedBox(height: 10,),
            const Text('Portfolio', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const ListTile(
              leading: Icon(Icons.web, color: Colors.orange),
              title: Text('https://pepperpotts.com'),
              subtitle: Text('Pepper Potts'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
            const SizedBox(height: 10,),
            const ListTile(
              leading: Icon(Icons.web, color: Colors.orange),
              title: Text('https://pepperpotts.com'),
              subtitle: Text('Pepper Potts'),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          ],
        ),
      ),
    );
  }
}
