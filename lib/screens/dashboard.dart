import 'package:assignment/screens/profile-page.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Dashboard', style: Theme.of(context).textTheme.headline6,),
        actions: <Widget>[
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()));
          }, icon: const Icon(Icons.dashboard_rounded)),
          IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Profile()),
                );
              },
              icon: const Icon(Icons.face)
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: [
              Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Current Activity',
                    style: Theme.of(context).textTheme.headline6,
                  )
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: 220,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.pinkAccent[700],
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text('Jobs applied this month',style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),),
                                ),
                                SizedBox(height: 10),
                                Text('0', style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 50,
                                  fontWeight: FontWeight.bold,
                                ),),
                                SizedBox(height: 10),

                              ]
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: 220,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.deepOrange[700],
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    'Profile Completion',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text('80%', style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 50,
                                  fontWeight: FontWeight.bold,
                                ),),
                                SizedBox(height: 10),

                              ]
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: 220,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.teal[700],
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text('Status',style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),),
                                ),
                                SizedBox(height: 10),
                                Text('Available', style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                ),),
                                SizedBox(height: 10),

                              ]
                          ),
                        ),
                      ),
                    ]
                )
            )
          ),
          Column(
            children: [
              Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        'Profile',
                        style: Theme.of(context).textTheme.headline6,
                      )
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.all(10),
                child: ButtonBar(
                  alignment: MainAxisAlignment.center,
                  
                  children: [
                    SizedBox(
                        width: 120,
                        child: ElevatedButton(
                          onPressed: (){},
                          child: const Text('Edit Profile'),
                        )
                    ),
                    SizedBox(
                        width: 120,
                        child: ElevatedButton(
                          onPressed: (){},
                          child: const Text('View'),
                        )
                    ),
                  ],
                ),
              )
            ],
          )
        ]
      ),
    );
  }
}
